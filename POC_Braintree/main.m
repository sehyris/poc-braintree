//
//  main.m
//  POC_Braintree
//
//  Created by Sehyris Campos on 4/20/17.
//  Copyright © 2017 Sehyris Campos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
